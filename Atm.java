package dz3.part1.task8;

/*
Реализовать класс “банкомат” Atm.
Класс должен:
● Содержать конструктор, позволяющий задать курс валют перевода
долларов в рубли и курс валют перевода рублей в доллары (можно
выбрать и задать любые положительные значения)
● Содержать два публичных метода, которые позволяют переводить
переданную сумму рублей в доллары и долларов в рубли
● Хранить приватную переменную счетчик — количество созданных
инстансов класса Atm и публичный метод, возвращающий этот счетчик
(подсказка: реализуется через static)
 */
public class Atm {
    private double currencyRateOfRoublesForDollars; // Курс покупки 1 доллара за рубли
    private double currencyRateOfDollarsForRoubles; // Курс покупки рублей за 1 доллар
    private double amountOfRoubles; // Сумма рублей
    private double amountOfDollars; // Сумма долларов

     private static int countOfInstance; // Счётчик количества созданных экземпляров класса

    public Atm(double currencyRateOfRoublesForDollars, double currencyRateOfDollarsForRoubles) {
       this.currencyRateOfRoublesForDollars = currencyRateOfRoublesForDollars;
       this.currencyRateOfDollarsForRoubles = currencyRateOfDollarsForRoubles;
       countOfInstance++;
    }

    public static int getCountOfInstance() {
        return countOfInstance;
    }

    public double getCurrencyRateOfRoublesForDollars() { // Геттер - метод, который выводит курс покупки 1 доллара в рублях(если такое понадобится потом, отходя от условий задачи)
        return currencyRateOfRoublesForDollars;
    }

    public double getCurrencyRateOfDollars() { // Геттер - метод, который выводит курс продажи 1 доллара в рублях(если такое понадобится потом, отходя от условий задачи)
         return currencyRateOfDollarsForRoubles;
    }

    /**
     * Метод, переводящий сумму рублей в сумму долларов по указанному курсу в конструкторе
     * @param amountOfRoubles Общая сумма рублей, которая будет переведена в доллары
     * @return
     */
    public double convertFromRoublesToDollars(double amountOfRoubles) {
        return this.amountOfDollars = amountOfRoubles / currencyRateOfRoublesForDollars;
    }

    /**
     * Метод, переводящий сумму долларов в рубли по указанному курсу в конструкторе
     * @param amountOfDollars Общая сумма долларов, которая будет переведена в рубли
     * @return
     */
    public double convertFromDollarsToRoubles(double amountOfDollars) {
        return this.amountOfRoubles = amountOfDollars * currencyRateOfDollarsForRoubles;
    }
}
