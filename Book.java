package dz3.part2;

import java.util.Objects;

public class Book {
    private String nameOfTheBook; // Название книги
    private String author;  // Автор книги
    private boolean theBookBorrowed; // Заимствована книга или нет
    private int scoreOfTheBook; // Оценка книги
    private int countOfScoreBook; // Кол-во оценок книги
    private double totalPointsOfTheBook; // Общее количество баллов книги

    public Book() {

    }

    public Book(String nameOfTheBook, String author) {
        this.nameOfTheBook = nameOfTheBook;
        this.author = author;
    }

    /**
     * Метод возвращает название книги
     * @return
     */
    public String getNameOfTheBook() {
        return nameOfTheBook;
    }

    /**
     * Метод устанавливающий название книги
     * @param name Название книги
     */
    public void setNameOfTheBook(String name) {
        this.nameOfTheBook =  name;
    }

    /**
     * Метод возвращает ФИО автора(в последовательности: имя, отчество, фамилия)
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Метод устанавливает ФИО автора(в последовательности: имя, отчество, фамилия)
     * @param author ФИО автора
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Метод возвращает значение true / false в зависимости от того заимствована книга или нет
     * @return
     */
    public boolean isTheBookBorrowed() {
        return  theBookBorrowed;
    }

    /**
     * Метод устанавливает значение true / false для поля данных заимствована книга или нет
     * @param theBookBorrowed
     */
    public void setTheBookBorrowed(boolean theBookBorrowed) {
        this.theBookBorrowed = theBookBorrowed;
    }

    /**
     * Метод возвращает оценку книги
     * @return
     */
    public int getScoreOfTheBook() {
        return scoreOfTheBook;
    }

    /**
     * Метод устанавливает оценку книге, прибавляет эту оценку к общей сумме оценок по этой книге и увеличивает счётчик оценок на 1.
     * @param scoreOfTheBook Оценка книги
     */
    public void setScoreOfTheBook(int scoreOfTheBook) {
        if (scoreOfTheBook >= 0 && scoreOfTheBook < 6) {
            this.scoreOfTheBook = scoreOfTheBook;
            this.totalPointsOfTheBook += scoreOfTheBook;
            this.countOfScoreBook++;
        }

    }

    /**
     * Метод возвращает количество оценок книги
     * @return
     */
    public int getCountOfScoreBook() {
        return countOfScoreBook;
    }

    /**
     * Метод возвращает сумму всех оценок по книге
     * @returnй
     */
    public double getTotalPointsOfTheBook() {
        return totalPointsOfTheBook;
    }

    public void setCountOfScoreBook(int countOfScoreBook) {
        this.countOfScoreBook = countOfScoreBook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return nameOfTheBook.equals(book.nameOfTheBook);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfTheBook);
    }
}
