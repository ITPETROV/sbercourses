package dz3.part1.task2;

public class Student {
    private String firstname; // Имя студента
    private String twoname; // Фамилия студента
    private int[]  Extim = new int[10]; // Последние 10 оценок студента. grades = new int[10];


    public String getFirstname() {

        return firstname;
    }

    public void setFirstname(String name) {

        this.firstname = name;
    }

    public String getTwoname() {

        return twoname;
    }

    public void setTwoname(String surname) {

        this.twoname = surname;
    }

    public int[] getExtim () {
        return Extim;
    }

    public void setExtim(int[] extim) {
        this.Extim = extim;
    }

    /**
     * Метод, добавляющий новую оценку в конец массива и удаляющий самую первую оценку(происходит сдвиг массива влево на 1)
     * @param Extim Массив, в котором необходимо сделать изменение
     * @param score Оценка, которую необходимо добавить в конец массива
     * @return
     */
    public void setScore(int[] Extim, int score) {
        for (int i = 0; i < Extim.length - 1; i++) {
            int temp = Extim[i + 1];
            Extim[i] = temp;

        }
        Extim[Extim.length - 1] = score;
    }
    public double getAverageStudentScore() {
        double total = 0; // Переменная для суммирования всех оценок массива
        double averageScore = 0; // Переменная для вычисления среднего арифметического от всех оценок в массиве
        for (int i = 0; i < Extim.length; i++) {
            total += Extim[i];
        }
        return averageScore = ((int)(total / Extim.length * 10)) / 10.0;
    }
}
