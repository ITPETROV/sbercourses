package dz3.part1.task2;

import java.util.Arrays;
public class TestStudent {
    public static void main(String[] args) {
        Student student =  new Student();
        student.setFirstname("Иван");
        student.setTwoname("Иванов");
        int[] scoreStudent = {3,4,4,3,5,5,4,5,5,};
        student.setExtim(scoreStudent);
        student.setScore(scoreStudent, 4);

        System.out.println("Имя студента " + student.getFirstname() + ", фамилия студента " + student.getTwoname() + ", средний арифметический бал студента исходя его последних оценок - " + student.getAverageStudentScore());

        // Для наглядности того, что оценки сдвигаются и последняя оценка изменяется
        System.out.println("Последние оценки студента: " + Arrays.toString(student.getExtim()));
    }
}
