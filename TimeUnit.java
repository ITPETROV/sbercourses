package dz3.part1.task4;


/*
 */
public class TimeUnit {
    private int hour;
    private int minute;
    private int second;


    /**
     * Конструктор по умолчанию
     */
    public TimeUnit() {

    }

    /**
     * Конструктор, который создаёт объект с 3 параметрами
     * @param hour Часы
     * @param minute Минуты
     * @param second Секунды
     */
    public TimeUnit(int hour, int minute, int second) {
        // Валидации входных данных
        if (hour >= 0 && hour < 24 ) {
            this.hour = hour;
        }
        else {
            this.hour = hour - 24;
        }
        if (minute >=0 && minute < 61) {
            this.minute = minute;
        }
        else {
            this.minute = minute = - 60;
        }
        if (second >= 0 && second < 61) {
            this.second = second;
        }
        else {
            this.second = second - 60;
        }
    }

    /**
     * Конструктор, который создаёт объект с 2 параметрами: часы, минуты (значение поля данных seconds = 0)
     * @param hour Часы
     * @param minute Минуты
     */
    public TimeUnit(int hour, int minute) {
        // Валидации входных данных
        if (hour >= 0 && hour < 24 ) {
            this.hour = hour;
        }
        else {
            System.out.println("Некорректное количество часов");
        }
        if (minute >= 0 && minute < 61) {
            this.minute = minute;
        }
        else {
            System.out.println("Некорректное количество минут");
        }
        this.second = 0;
    }

    /**
     * Конструктор, который создаёт объект с 1-им параметром: часы (значения полей данных minutes и seconds равны 0)
     * @param hour Часы
     */
    public TimeUnit(int hour) {
        // Валидация входных данных
        if (hour >= 0 && hour < 24 ) {
            this.hour = hour;
        }
        else {
            System.out.println("Некорректное количество часов");
        }
        this.minute = 0;
        this.second = 0;
    }

    // Геттер - методы для приватных полей данных(переменных экземпляра)
    public int getHours() {
            return this.hour;
    }

    public int getMinutes() {
        return this.minute;
    }

    public int getSeconds() {
        return this.second;
    }

    /**
     * Метод, выводящий на экран установленное в классе время в формате hh:mm:ss
     */
    public void getTime() {
        System.out.printf("%02d:%02d:%02d" + "\n", getHours(), getMinutes(), getSeconds());
    }

    /**
     * Метод, выводящий на экран установленное в классе время в 12 - часовом формате(используя am / pm)
     */
    public void getAmPmTime() {
        if (getHours() > 12) {
            System.out.printf("%02d:%02d:%02d pm" + "\n",(getHours() - 12), getMinutes(), getSeconds());
        }
        else {
            System.out.printf("%02d:%02d:%02d am" + "\n", getHours(), getMinutes(), getSeconds());
        }
    }


    /**
     * Метод, который прибавляет переданное время к установленному(на вход подаются только часы, минуты и секунды)
     * @param hours Часы
     * @param minutes Минуты
     * @param seconds Секунды
     */
    public void setTime(int hours, int minutes, int seconds) {
       this.hour = getHours() + hours;
       if (this.hour > 23) {
           this.hour = this.hour - 24;
       }
       this.minute = getMinutes() + minutes;
       if (this.minute > 60) {
           this.minute = this.minute - 60;
           this.hour = this.hour + 1;
       }
       this.second = getSeconds() + seconds;
       if (this.second > 60) {
           this.second = this.second - 60;
           this.minute = this.minute + 1;
       }
    }
}
