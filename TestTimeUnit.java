package dz3.part1.task4;

public class TestTimeUnit {
    public static void main(String[] args) {
        TimeUnit timing1 = new TimeUnit(23,7,59);
        TimeUnit timing2 =  new TimeUnit(8);

        timing1.getTime();
        timing2.setTime(3,15,25);
        timing1.getTime();

        timing1.getTime();
        timing2.getAmPmTime();
        timing2.setTime(10, 25, 34);
        timing2.getAmPmTime();

    }

}
