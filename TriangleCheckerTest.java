package dz3.part1.task7;

public class TriangleCheckerTest {
    public static void main(String[] args) {

        // Тест для проверки работоспособности метода, который выводит true
        System.out.println(TriangleChecker.isTriangle(1,2,2));

        // Тест для проверки работоспособности метода, который выводит false
        System.out.println(TriangleChecker.isTriangle(1,2,5));

        // Ещё одна проверка работоспособности метода, которая выводит true
        System.out.println(TriangleChecker.isTriangle(5,5,5));

        // Ещё одна проверка работоспособности метода, которая выводит false
        System.out.println(TriangleChecker.isTriangle(4,5,15));
    }
}
