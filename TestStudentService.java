package dz3.part1.task3;

import dz3.part1.task2.Student;

public class TestStudentService {
    public static void main(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student();
        students[0].setFirstname("Семён");
        students[0].setTwoname("Слепаков");
        int[] arr1 = {2, 3, 5, 5, 5, 4, 3, 3, 3, 2};
        students[0].setExtim(arr1);

        students[1] = new Student();
        students[1].setFirstname("Пётр");
        students[1].setTwoname("Петров");
        int[] arr2 = {2, 3, 3, 3, 3, 4, 4, 5, 2, 2};
        students[1].setExtim(arr2);

        students[2] = new Student();
        students[2].setFirstname("Иван");
        students[2].setTwoname("Иванов");
        int[] arr3 = {5, 5, 5, 5, 4, 4, 4, 4, 4, 4};
        students[2].setExtim(arr3);

        StudentService list = new StudentService();
        System.out.println("Лучший студент по самому высокому среднему баллу: " + list.bestStudent(students).getFirstname() + " " + list.bestStudent(students).getTwoname());

        list.sortBySurname(students);
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].getTwoname());
        }

    }
}
