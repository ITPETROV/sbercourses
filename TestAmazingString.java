package dz3.part1.task6;

import java.util.Arrays;

public class TestAmazingString {
    public static void main(String[] args) {

        char[] arr = {'s', 't', 'r', 'o', 'k','a'};
        char[] substring = {'r', 'o', 'k'};
        AmazingString test = new AmazingString(arr);
        System.out.println(test.getChar(2)); // Проверка работоспособности метода, который выводит элемент по заданному индексу
        System.out.println(test.getLengthString()); // Проверка работоспособности метода, который выводит длину строки, являющаяся массивом символов
        test.getOurString(); // Проверка работоспособности метода, который выводит нашу строку, являющаяся массивом символов
        System.out.println(test.foundSubstringFromArray(substring)); // Проверка работоспособности метода, который проверяет содержится ли переданная подстрока(которая подаётся через массив char) в AmazingString
        // и в зависимости от ответа выводит true / false
        System.out.println(test.foundSubstringFromString("oka")); // Проверка работоспособности метода, который проверяет содержится ли переданная подстрока(которая подаётся через массив char) в AmazingString
        // и в зависимости от ответа выводит true / false

        AmazingString test1 = new AmazingString("cat");
        test1.getOurString(); // Показываем, что выводится строка с пробелами
        test1.deleteSpace(); // Удаляем пробелы
        test1.getOurString();// Показываем, что пробелы удалились
        test.getOurString(); // Выводим наш массив
        test.reverseString(); // Делаем ревёрс массива
        test.getOurString(); // Показываем ревёрс строки








    }

}

