package dz3.part2;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Library {

    List<Book> nameOfTheBook = new ArrayList<>(); // Список существующих книг в библиотеке(сюда входят все добавленные книги, в том числе и одолженные)

    /**
     * Метод, который добавляет книгу в список книг библиотеки
     *
     * @param book Ссылочная переменная, которая хранит в себе путь на объект типа Book
     */
    public void addBook(Book book) {
        if (!nameOfTheBook.contains(book.getNameOfTheBook())) {
            nameOfTheBook.add(book);
        }
    }

    /**
     * Метод, который удаляет книгу из библиотеки по названию, если книга в принципе есть в библиотеке и она в настоящий момент не одолжена.
     *
     * @param nameBook      Название книги
     */
    public void removeBook(String nameBook) {
        for (int i = 0; i < nameOfTheBook.size(); i++) {
            if (nameOfTheBook.get(i).getNameOfTheBook().equals(nameBook)) {
                if(!nameOfTheBook.get(i).isTheBookBorrowed()) {
                    nameOfTheBook.remove(nameOfTheBook.get(i));
                }
            }
        }
    }


    /**
     * Метод, который находит и возвращает книгу по названию
     *
     * @param nameBook      Название книги
     * @return
     */
    public Book getBook(String nameBook) {
        for (int i = 0; i < nameOfTheBook.size(); i++) {
            if (nameOfTheBook.get(i).getNameOfTheBook().equals(nameBook)) {
                return nameOfTheBook.get(i);
            }
        }
        return null;
    }

    /**
     * Метод, который находит и возвращает список книг по автору
     *
     * @param author        Автор книг, по которому мы будем искать все его книги в библиотеки
     * @return
     */
    public ArrayList<Book> getBooksOfAuthor(String author) {
        ArrayList<Book> authorOfBooks = new ArrayList<>(); // Создаём список типа Book для хранения всех книг одного автора
        // Если происходит совпадение автора книги в нашей библиотеки и автора, полученного в качестве аргумента, то книга добавляется в новый созданный список
        for (int i = 0; i < nameOfTheBook.size(); i++) {
            if (nameOfTheBook.get(i).getAuthor().equals(author)) {
                authorOfBooks.add(nameOfTheBook.get(i));
            }
        }
        return authorOfBooks;
    }

    /**
     * Метод, который одалживает книгу посетителю если в данный момент у него нет книги. В случае если посетитель обращается за книгой в первый раз, то ему выдаётся уникальный идентификатор посетителя.
     *
     * @param nameBook      Название книги
     * @param visitor       Посетитель
     */
    public void getBookForVisitor(String nameBook, Visitor visitor) {
        if (visitor.getFirstTimeInTheLibrary()) { // Проверяем первый ли раз посетитель пришёл в библиотеку
            visitor.setId(Visitor.getCountVisitor());
        }
        for (int i = 0; i < nameOfTheBook.size(); i++) {
            if (nameOfTheBook.get(i).getNameOfTheBook().equals(nameBook)) {
                if (visitor.getWhichBookVisitorHasNow() == null) {
                    if (!nameOfTheBook.get(i).isTheBookBorrowed()) {
                        visitor.setWhichBookVisitorHasNow(nameOfTheBook.get(i));
                        nameOfTheBook.get(i).setTheBookBorrowed(true);
                        break;
                    } else {
                        System.out.println("Такая книга уже находится у другого посетителя. Выберите другую.");
                        break;
                    }
                } else {
                    System.out.println("У вас уже есть книга. Для того, чтобы получить новую книгу, необходимо сдать книгу, которая у вас на руках.");
                    break;
                }

                }
            else {
                System.out.println("В библиотеке такой книги нет. Выберите другую.");
                break;
            }
            }
        }

    /**
     * Метод возвращает посетителем книгу в библиотеку и оценивает её по 5-тя бальной шкале(от 0 до 5).
     * @param book Книга, которую посетитель возвращает
     * @param visitor Посетитель
     */
    public void returnTheBook(Book book, Visitor visitor) {
        if (visitor.getWhichBookVisitorHasNow().getNameOfTheBook().equals(book.getNameOfTheBook())) { // Проверяем есть ли эта книга у посетителя, которую он хочет вернуть
            visitor.setWhichBookVisitorHasNow(null);
            book.setTheBookBorrowed(false);
            System.out.print(visitor.getNameVisitor() + ", оцените, пожалуйста, книгу по 5-ти бальной шкале(от 0 до  5): ");
            Scanner scan = new Scanner(System.in);
            int score = 0; // Переменная для получения оценки книги от посетителя
            // Цикл для проверки соблюдения условия выставления оценки(она должна быть от 0 до 5). Если условие не соблюдено, то оценка вводится заново
            do {
                score = scan.nextInt();
                if (score > 5 || score < 0) {
                    System.out.println("Вы некорректно оценили книгу. Шкала оценивая от 0 до 5. Введите оценку ещё раз.");
                }
            } while (score > 5 || score < 0);
            book.setScoreOfTheBook(score); // Присваивается оценка книге
        }
        else {
            System.out.println("Вы хотите сдать книгу, которой у вас нет");
        }
    }

    /**
     * Метод, который возвращает среднюю арифметическую оценку книги
     * @param nameBook Название книги
     * @return
     */
    public double getAverageScoreOfTheBook(String nameBook) {
        // Если такая книга в библиотеке есть, то выводится её среднее арифметическое.
        for (int i = 0; i < nameOfTheBook.size(); i++) {
            if (nameOfTheBook.get(i).getNameOfTheBook().equals(nameBook)) {
                if (nameOfTheBook.get(i).getCountOfScoreBook() == 0) { // Если у этой книги не было оценок, то выводим 0.0
                    return 0.0;
                }
                else {
                    return (nameOfTheBook.get(i).getTotalPointsOfTheBook() / nameOfTheBook.get(i).getCountOfScoreBook() * 100) / 100.0;
                }
            }
        }
        // Если такой книги в библиотеке не оказалось
        System.out.print("Книги " + "\"" + nameBook + "\"" + " в библиотеке нет.");
        return -1.0;
    }

    public void printOurList() {
        for (int i = 0; i < nameOfTheBook.size(); i++) {
            System.out.println("Название книги: " + "\"" + nameOfTheBook.get(i).getNameOfTheBook() + "\"" + ", автор книги: " + nameOfTheBook.get(i).getAuthor());
        }
    }
}


