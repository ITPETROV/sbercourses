package dz3.part1.task1;

public class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        double rand = (int)(1 + Math.random() * 3);
        if (rand == 1) {
            sleep();
        }
        else if (rand == 2) {
            meow();
        }
        else if (rand == 3){
            eat();
        }
    }

}
