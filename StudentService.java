package dz3.part1.task3;

import dz3.part1.task2.Student;
import java.util.Arrays;

/*
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.

 */
public class StudentService {

    public Student bestStudent(Student[] students) {

        double maxAverageScore = students[0].getAverageStudentScore();
        int indexBestStudents = 0;

        for (int i = 0; i < students.length; i++) {
            if (students[i].getAverageStudentScore() > maxAverageScore) {
                maxAverageScore = students[i].getAverageStudentScore();
                indexBestStudents = i;
            }
        }
        return students[indexBestStudents];
    }

    public void sortBySurname(Student[] students) {
     String[] surnameOfStudents = new String[students.length]; // Создаём массив типа String для внесения туда фамилий студентов
     for (int i = 0; i < students.length; i++) {
         surnameOfStudents[i] = students[i].getTwoname();
     }
     Arrays.sort(surnameOfStudents); // Сортируем массив фамилий студентов

        // Во вложенных циклах проверяем совпадения фамилий и в случае совпадения меняем студентов
        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length; j++) {
                if (surnameOfStudents[i].equals(students[j].getTwoname())) {
                    Student[] temp = {students[i]}; // Массив для хранения объекта типа Student
                    students[i] = students[j];
                    students[j] = temp[0];
                }
            }
        }
    }
}
