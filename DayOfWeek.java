package dz3.part1.task5;

/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */
public class DayOfWeek {
    private byte DaysOfNumber; // Порядковый номер для недели
    private String name; // Название дня недели


    public byte getNumberOfDay() {
        return DaysOfNumber;
    }

    public void setNumberOfDay(byte DaysOfNumber) {
        this.DaysOfNumber = DaysOfNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
