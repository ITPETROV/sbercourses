package dz3.part1.task7;

/*
Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false. Входные длины сторон треугольника — числа типа
double. Придумать и написать в методе main несколько тестов для проверки
работоспособности класса (минимум один тест на результат true и один на
результат false)

 */
public class TriangleChecker {
    public static boolean isTriangle(double firstSide, double secondSide, double thirdSide) {
        return firstSide + secondSide > thirdSide && firstSide + thirdSide > secondSide && secondSide + thirdSide > firstSide;
    }
}
