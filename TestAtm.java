package dz3.part1.task8;

public class TestAtm {
    public static void main(String[] args) {
        // Создаём объект и задаём ему курсы валют
        Atm atm1 = new Atm(65.56, 78.34);
        System.out.println("Сумма долларов составила: " + (int)(atm1.convertFromRoublesToDollars(7250) * 100) / 100.0);
        System.out.println("Сумма рублей составила: " + (int)(atm1.convertFromDollarsToRoubles(624) * 100) / 100.0);
        System.out.println("Количество созданных объектов равно: " + Atm.getCountOfInstance()); // Выводит количество созданных объектов

        // Создаём второй объект и задаём ему новые курсы валют
        Atm atm2 =  new Atm(60.23, 75.56);
        System.out.println("Сумма долларов составила: " + (int)(atm2.convertFromRoublesToDollars(7250) * 100) / 100.0);
        System.out.println("Сумма рублей составила: " + (int)(atm2.convertFromDollarsToRoubles(624) * 100) / 100.0);
        System.out.println("Количество созданных объектов равно: " + Atm.getCountOfInstance()); // Выводит количество созданных объектов
    }
}
