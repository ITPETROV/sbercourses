package dz3.part1.task5;

public class TestDayOfWeek {
    public static void main(String[] args) {
        DayOfWeek[] days = new DayOfWeek[7];

        for (int i = 0; i < days.length; i++) {
            days[i] = new DayOfWeek();
            days[i].setNumberOfDay((byte)(i + 1));
        }

        days[0].setName("Monday");
        days[1].setName("Tuesday");
        days[2].setName("Wednesday");
        days[3].setName("Thursday");
        days[4].setName("Friday");
        days[5].setName("Saturday");
        days[6].setName("Sunday");

        for (int i = 0; i < days.length; i++) {
            System.out.println(days[i].getNumberOfDay() + " " + days[i].getName());
        }
    }
}
